package com.hh.eng_learner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EngLearnerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EngLearnerApplication.class, args);
	}

}
