package com.hh.eng_learner.dto;

import java.time.LocalDate;

public class CourseDto {
    private Long id;
    private String name;
    private String cerf;
    private String creator;
    private int version;
    private LocalDate publication;
    private String description;

}
