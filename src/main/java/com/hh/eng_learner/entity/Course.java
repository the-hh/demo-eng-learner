package com.hh.eng_learner.entity;

import com.hh.eng_learner.validation.Cerf;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "course")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "name cannot be blank")
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull(message = "level cannot be null")
    @Column(name = "level", nullable = false)
    private String level;

    @Cerf(message = "cerf must be A1, A2, A3, B1, B2, B3, C1, C2 or C3")
    @Column(name = "cerf", nullable = false)
    private String cerf;

    @NotBlank(message = "creator cannot be blank")
    @NotNull(message = "creator cannot be null")
    @Column(name = "creator", nullable = false)
    private String creator;


    @NotNull(message = "version cannot be null")
    @Column(name = "version", nullable = false)
    private int version;

    @NotNull(message = "publication cannot be null")
    @Column(name = "publication", nullable = false)
    private LocalDate publication;

    @NotBlank(message = "description cannot be blank")
    @Column(name = "description", nullable = false)
    private String description;

    @ManyToMany
    @JoinTable(
            name = "course_tagger",
            joinColumns = @JoinColumn(name = "course_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tagger_id", referencedColumnName = "id")
    )
    private Set<Tagger> taggers = new HashSet<>();


    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    private Set<Unit> units = new HashSet<>();

}

