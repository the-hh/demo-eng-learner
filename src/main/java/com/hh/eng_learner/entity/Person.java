package com.hh.eng_learner.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="persons")
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;
}
