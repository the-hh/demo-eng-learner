package com.hh.eng_learner.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tagger")
public class Tagger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank(message = "tagger cannot be blank")
    @NotNull
    @Column(name = "tagger", nullable = false, unique = true)
    private String tagger;

    @NotBlank(message = "Description cannot be blank")
    @NonNull
    @Column(name = "description", nullable = false)
    private String description;

//    @JsonIgnore
//    @ManyToMany
//    @JoinTable(
//            name = "course_tagger",
//            joinColumns = @JoinColumn(name = "tagger_id", referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "course_id", referencedColumnName = "id")
//    )
//    private Set<Course> courses;

}
