package com.hh.eng_learner.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(
        name = "unit",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"sequense_number", "course_id"})}
)
public class Unit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull(message = "sequenseNumber cannot be null")
    @Column(name = "sequense_number", nullable = false)
    @Min(0)
    @Max(100)
    private int sequenseNumber;

    @NotBlank(message = "name cannot be blank")
    @NotNull
    @Size(max = 100)
    @Column(name = "topic", nullable = false)
    private String topic;

    @NotBlank(message = "reading cannot be blank")
    @NotNull
    @Size(max = 1000)
    @Column(name = "reading", nullable = false)
    private String reading;

    @JsonIgnore
    @ManyToOne(optional = false)
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    private Course course;


    @OneToMany(mappedBy = "unit", cascade = CascadeType.ALL)
    private Set<Word> wordSet = new HashSet<>();



}


