package com.hh.eng_learner.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "word")
public class Word {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank(message = "Tagger cannot be blank")
    @NotNull
    @Column(name = "tagger")
    private String tagger;

    @NotBlank(message = "inEstonian cannot be blank")
    @NotNull
    @Size(max = 255, message = "inEstonian max length is 255")
    @Column(name = "in_estonian")
    private String inEstonian;

    @NotBlank(message = "inEnglish cannot be blank")
    @NotNull
    @Size(max = 255, message = "inEnglish max length is 255")
    @Column(name = "in_english")
    private String inEnglish;

    @JsonIgnore
    @ManyToOne(optional = false)
    @JoinColumn(name = "unit_id", referencedColumnName = "id")
    private Unit unit;


}

