package com.hh.eng_learner.repository;

import com.hh.eng_learner.entity.Tagger;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface TaggerRepository extends CrudRepository<Tagger, Long> {

    // Query example:
    @Query(nativeQuery = true, value = "SELECT * FROM TAGGER WHERE TAGGER LIKE :tagger_name")
    Optional<Tagger> findByTaggerName(@Param("tagger_name") String tagger);

//    Better solution:
//    Optional<Tagger> findByTagger( String Tagger);

}
