package com.hh.eng_learner.repository;

import com.hh.eng_learner.entity.Word;
import org.springframework.data.repository.CrudRepository;


public interface WordRepository extends CrudRepository<Word, Long> {

}
