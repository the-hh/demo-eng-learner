package com.hh.eng_learner.repository;

import com.hh.eng_learner.entity.Word;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WordsRepository  extends PagingAndSortingRepository<Word, Long> {

}
