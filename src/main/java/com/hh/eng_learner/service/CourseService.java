package com.hh.eng_learner.service;



import com.hh.eng_learner.entity.Course;
import com.hh.eng_learner.entity.Unit;

import java.util.List;
import java.util.Set;


public interface CourseService {
    Course getCourse(Long id);
    Course saveCourse(Course course);
    void deleteCourse(Long id);
    List<Course> getCourses();

    Course addTaggerToCourse(Long courseId, Long taggerId);

    Unit addUnit(Long courseId, Unit unit);

    Set<Unit> getUnits(Long courseId);
//    Set<Student> getEnrolledStudents(Long id);
}
