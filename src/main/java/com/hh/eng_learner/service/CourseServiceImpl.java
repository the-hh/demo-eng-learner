package com.hh.eng_learner.service;


import com.hh.eng_learner.entity.Course;
import com.hh.eng_learner.entity.Tagger;
import com.hh.eng_learner.entity.Unit;
import com.hh.eng_learner.exception.EntityNotFoundException;
import com.hh.eng_learner.repository.CourseRepository;
import com.hh.eng_learner.repository.TaggerRepository;
import com.hh.eng_learner.repository.UnitRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@Service
public class CourseServiceImpl implements CourseService {

    CourseRepository courseRepository;

    TaggerRepository taggerRepository;

    UnitRepository unitRepository;

    @Override
    public Course getCourse(Long id) {
        Optional<Course> course = courseRepository.findById(id);
        if(course.isPresent()) {
            return course.get();
        } else throw new EntityNotFoundException(id, Course.class);
    }

    @Override
    public Course saveCourse(Course course) {
        return courseRepository.save(course);
    }

    @Override
    public void deleteCourse(Long id) {
        Optional<Course> course = courseRepository.findById(id);
        if(course.isPresent()) {
            courseRepository.deleteById(id);
        } else throw new EntityNotFoundException(id, Course.class);
    }

    @Override
    public List<Course> getCourses() {
        return courseRepository.findAll();
    }

    @Override
    public Course addTaggerToCourse(Long courseId, Long taggerId) {
        Course course = getCourse(courseId);
        Optional<Tagger> optionalTagger = taggerRepository.findById(taggerId);
        if (optionalTagger.isPresent()) {
            Tagger foundTagger = optionalTagger.get();
            course.getTaggers().add(foundTagger);
            return courseRepository.save(course);
        } else throw new EntityNotFoundException(taggerId, Tagger.class);
    }

    @Override
    public Unit addUnit(Long courseId, Unit unit) {
        Optional<Course> optionalCourse = courseRepository.findById(courseId);
        if(optionalCourse.isPresent()) {
            Course course = optionalCourse.get();
            unit.setCourse(course);
            return unitRepository.save(unit);
        } else throw new EntityNotFoundException(courseId, Course.class);
    }

    @Override
    public Set<Unit> getUnits(Long courseId) {
        Optional<Course> optionalCourse = courseRepository.findById(courseId);
        if(optionalCourse.isPresent()) {
            return optionalCourse.get().getUnits();
        } else throw new EntityNotFoundException(courseId, Course.class);
    }


}
