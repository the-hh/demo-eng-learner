package com.hh.eng_learner.service;



import com.hh.eng_learner.entity.Tagger;

import java.util.List;


public interface TaggerService {

    Tagger saveTagger(Tagger tagger);

    List<Tagger> getTaggers();

    Tagger getTaggerById(Long id);

    Tagger updateTagger(Long id, Tagger tagger);

    void deleteTaggerById(Long id);

    Tagger findByTagger(String tagger);
}

