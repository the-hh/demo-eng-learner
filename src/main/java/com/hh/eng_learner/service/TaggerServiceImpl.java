package com.hh.eng_learner.service;

import com.hh.eng_learner.entity.Tagger;
import com.hh.eng_learner.exception.EntityNotFoundException;
import com.hh.eng_learner.repository.TaggerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TaggerServiceImpl implements TaggerService {

    TaggerRepository taggerRepository;
    @Override
    public Tagger saveTagger(Tagger tagger) {
        return taggerRepository.save(tagger);
    }

    @Override
    public List<Tagger> getTaggers() {
        return (List<Tagger>) taggerRepository.findAll();
    }

    @Override
    public Tagger getTaggerById(Long id) {
        Optional<Tagger> optionalTagger = taggerRepository.findById(id);
        if(optionalTagger.isPresent()) {
            return optionalTagger.get();
        } else throw new EntityNotFoundException(id, Tagger.class);
    }

    @Override
    public Tagger updateTagger(Long id, Tagger tagger) {
        Optional<Tagger> optionalTagger = taggerRepository.findById(id);
        if(optionalTagger.isPresent()) {
            tagger.setId(id);
            return taggerRepository.save(tagger);
        } else throw new EntityNotFoundException(id, Tagger.class);
    }

    @Override
    public void deleteTaggerById(Long id) {
        Optional<Tagger> optionalTagger = taggerRepository.findById(id);
        if(optionalTagger.isPresent()) {
            taggerRepository.deleteById(id);
        } else throw new EntityNotFoundException(id, Tagger.class);
    }

    @Override
    public Tagger findByTagger(String tagger) {
        Optional<Tagger> optionalTagger = taggerRepository.findByTaggerName(tagger);
        if(optionalTagger.isPresent()) {
            return optionalTagger.get();
        } else throw new EntityNotFoundException(tagger, Tagger.class);
    }

}
