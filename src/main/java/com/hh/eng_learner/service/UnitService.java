package com.hh.eng_learner.service;

import com.hh.eng_learner.entity.Unit;
import com.hh.eng_learner.entity.Word;

import java.util.List;

public interface UnitService {
    Unit saveUnit(Unit unit);

    List<Unit> getUnitsByCourseId();

    Unit saveUnitWords(Long unitId, List<Word> words);
}
