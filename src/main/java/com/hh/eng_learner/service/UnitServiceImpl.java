package com.hh.eng_learner.service;


import com.hh.eng_learner.entity.Tagger;
import com.hh.eng_learner.entity.Unit;
import com.hh.eng_learner.entity.Word;
import com.hh.eng_learner.exception.EntityNotFoundException;
import com.hh.eng_learner.repository.UnitRepository;
import com.hh.eng_learner.repository.WordRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class UnitServiceImpl implements UnitService {

    UnitRepository unitRepository;
    WordRepository wordRepository;
    @Override
    public Unit saveUnit(Unit unit) {
        return unitRepository.save(unit);
    }

    @Override
    public List<Unit> getUnitsByCourseId() {
        return (List<Unit>) unitRepository.findAll();
    }

    @Override
    public Unit saveUnitWords(Long unitId, List<Word> words) {
        Optional<Unit> optionalUnit = unitRepository.findById(unitId);
        if(optionalUnit.isPresent()) {
            Unit unit = optionalUnit.get();

            wordRepository.deleteAll(unit.getWordSet());
            unit.getWordSet().clear();

            List<String> taggers = unit.getCourse().getTaggers().stream().map(Tagger::getTagger).toList();

            List<Word> wordList = words.stream()
                    .filter(w -> taggers.contains(w.getTagger()))
                    .toList();

            for (Word word: wordList) {
                word.setUnit(unit);
                word = wordRepository.save(word);
                unit.getWordSet().add(word);
            }

            return unitRepository.save(unit);
        } else throw new EntityNotFoundException(unitId, Unit.class);
    }
}

