package com.hh.eng_learner.service;


import com.hh.eng_learner.entity.Word;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface WordsService {

    Page<Word> findAll(Pageable pageable);

}
