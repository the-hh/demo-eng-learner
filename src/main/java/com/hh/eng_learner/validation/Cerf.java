package com.hh.eng_learner.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CerfValidator.class)

public @interface Cerf {

    String message() default "The skill level is not valid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}

