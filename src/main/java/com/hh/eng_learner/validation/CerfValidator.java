package com.hh.eng_learner.validation;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CerfValidator implements ConstraintValidator<Cerf, String> {

    List<String> levels = Arrays.asList("A0", "A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3");

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) return false;
        for (String level : levels) {
            if (value.equals(level)) return true;
        }
        return false;
    }

}

