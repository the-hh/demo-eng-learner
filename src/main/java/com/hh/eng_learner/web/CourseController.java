package com.hh.eng_learner.web;


import com.hh.eng_learner.entity.Course;
import com.hh.eng_learner.entity.Unit;
import com.hh.eng_learner.service.CourseService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@RestController
@RequestMapping("/course")
public class CourseController {

    CourseService courseService;

    @GetMapping("/{id}")
    public ResponseEntity<Course> getCourse(@PathVariable Long id) {
        return new ResponseEntity<>(courseService.getCourse(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Course> saveCourse(@Valid @RequestBody Course course) {
        return new ResponseEntity<>(courseService.saveCourse(course), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteCourse(@PathVariable Long id) {
        courseService.deleteCourse(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Course>> getCourses() {
        return new ResponseEntity<>(courseService.getCourses(), HttpStatus.OK);
    }

    @PutMapping("/{courseId}/tagger/{taggerId}")
    public ResponseEntity<Course> addTaggerToCourse(@PathVariable Long courseId, @PathVariable Long taggerId) {
        return new ResponseEntity<>(courseService.addTaggerToCourse(courseId,taggerId), HttpStatus.OK);
    }

    @PostMapping("/{courseId}/unit")
    public  ResponseEntity<Unit> addUnit(@PathVariable Long courseId, @Valid @RequestBody Unit unit) {
        return  new ResponseEntity<>(courseService.addUnit(courseId, unit) , HttpStatus.CREATED);
    }

    @GetMapping("/{courseId}/unit/all")
    public ResponseEntity<Set<Unit>> getUnits(@PathVariable Long courseId) {
        return new ResponseEntity<>(courseService.getUnits(courseId), HttpStatus.OK);
    }

}
