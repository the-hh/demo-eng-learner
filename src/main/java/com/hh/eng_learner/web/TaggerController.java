package com.hh.eng_learner.web;

import com.hh.eng_learner.entity.Tagger;
import com.hh.eng_learner.service.TaggerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/tagger")
public class TaggerController {

    TaggerService taggerService;

    @PostMapping
    public ResponseEntity<Tagger> saveTagger(@Valid @RequestBody Tagger tagger) {
        return new ResponseEntity<>(taggerService.saveTagger(tagger), HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Tagger>> getTaggers() {
        return new ResponseEntity<>(taggerService.getTaggers(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Tagger> getTaggerById(@PathVariable Long id) {
        return new ResponseEntity<>(taggerService.getTaggerById(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Tagger> updateTagger(@Valid @RequestBody Tagger tagger, @PathVariable Long id) {
        return new ResponseEntity<>(taggerService.updateTagger(id, tagger), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteTagger(@PathVariable Long id) {
        taggerService.deleteTaggerById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/name/{tagger}")
    public ResponseEntity<Tagger> getTaggerbyTaggerName(@PathVariable String tagger) {
        return new ResponseEntity<>(taggerService.findByTagger(tagger), HttpStatus.OK);
    }
}
