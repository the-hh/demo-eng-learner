package com.hh.eng_learner.web;


import com.hh.eng_learner.entity.Unit;
import com.hh.eng_learner.entity.Word;
import com.hh.eng_learner.service.UnitService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/unit")
public class UnitController {

    UnitService unitService;

    @PostMapping("/{unitId}/words")
    public ResponseEntity<Unit> saveUnitWords(@PathVariable Long unitId , @Valid @RequestBody List<Word> words) {
        return new ResponseEntity<>(unitService.saveUnitWords(unitId, words), HttpStatus.CREATED);
    }
}
