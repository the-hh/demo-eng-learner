package com.hh.eng_learner.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class CerfValidatorTest {

    private CerfValidator underTest;

    @BeforeEach
    void setUp() {
        underTest = new CerfValidator();
    }

    @ParameterizedTest
    @CsvSource({
            "A1, true",
            "C3, true",
            "H2, false",
            "A4, false"
    })
    void itShouldValidateSkillLevel(String skillLevel, boolean expected) {
        // Given CsvSource
        // When
        boolean isValid = underTest.isValid(skillLevel, null);

        // Then
        assertThat(isValid).isEqualTo(expected);

    }
}