package com.hh.eng_learner.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hh.eng_learner.entity.Course;
import com.hh.eng_learner.entity.Tagger;
import com.hh.eng_learner.entity.Unit;
import com.hh.eng_learner.service.CourseService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = CourseController.class)
class CourseControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    CourseService courseService;

    @Test
    void testGetCourse() throws Exception {
        Course course = new Course();
        course.setId(1L);
        course.setName("English World Level 2 Pupil's Book");
        course.setLevel("Level 3");
        course.setCerf("A2");
        course.setCreator("Macmillan Education");
        course.setVersion(1);
        course.setPublication(LocalDate.of(2016, 5, 4));
        course.setDescription("The Pupil's Book contains twelve units with each unit designed to be taught over a two-week period. The key skills of reading, writing, speaking and listening are consistently covered and are underpinned by the firm foundation of the grammar syllabus.");
        given(courseService.getCourse(Mockito.anyLong())).willReturn(course);


        mockMvc.perform(get("/course/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("name").value("English World Level 2 Pupil's Book"))
                .andExpect(jsonPath("level").value("Level 3"))
                .andExpect(jsonPath("cerf").value("A2"))
                .andExpect(jsonPath("creator").value("Macmillan Education"))
                .andExpect(jsonPath("version").value(1))
                .andExpect(jsonPath("publication").value("2016-05-04"))
                .andExpect(jsonPath("description").value("The Pupil's Book contains twelve units with each unit designed to be taught over a two-week period. The key skills of reading, writing, speaking and listening are consistently covered and are underpinned by the firm foundation of the grammar syllabus."))
                .andDo(print());
    }

    @Test
    void testSaveCourse() throws Exception {
        Course course = new Course();
        course.setId(1L);
        course.setName("English World Level 2 Pupil's Book");
        course.setLevel("Level 3");
        course.setCerf("A2");
        course.setCreator("Macmillan Education");
        course.setVersion(1);
        course.setPublication(LocalDate.of(2016, 5, 4));
        course.setDescription("The Pupil's Book contains twelve units with each unit designed to be taught over a two-week period. The key skills of reading, writing, speaking and listening are consistently covered and are underpinned by the firm foundation of the grammar syllabus.");
        given(courseService.saveCourse(Mockito.any())).willReturn(course);

        String json = mapper.writeValueAsString(course);

        mockMvc.perform(post("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andDo(print());
    }

    @Test
    void testDeleteCourse() throws Exception {
        mockMvc.perform(delete("/course/1"))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    void testGetCourses() throws Exception {
        Course course = new Course();
        course.setId(1L);
        course.setName("English World Level 2 Pupil's Book");
        course.setLevel("Level 3");
        course.setCerf("A2");
        course.setCreator("Macmillan Education");
        course.setVersion(1);
        course.setPublication(LocalDate.of(2016, 5, 4));
        course.setDescription("The Pupil's Book contains twelve units with each unit designed to be taught over a two-week period. The key skills of reading, writing, speaking and listening are consistently covered and are underpinned by the firm foundation of the grammar syllabus.");
        List<Course> courses = new ArrayList<>();
        courses.add(course);

        given(courseService.getCourses()).willReturn(courses);
        mockMvc.perform(get("/course/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andDo(print());
    }

    @Test
    void testAddTaggerToCourse() throws Exception {
        Tagger tagger = new Tagger();
        tagger.setId(1L);
        tagger.setTagger("first");
        tagger.setDescription("the first and its desc");

        Course course = new Course();
        course.setId(1L);
        course.setName("English World Level 2 Pupil's Book");
        course.setLevel("Level 3");
        course.setCerf("A2");
        course.setCreator("Macmillan Education");
        course.setVersion(1);
        course.setPublication(LocalDate.of(2016, 5, 4));
        course.setDescription("The Pupil's Book contains twelve units with each unit designed to be taught over a two-week period. The key skills of reading, writing, speaking and listening are consistently covered and are underpinned by the firm foundation of the grammar syllabus.");

        course.getTaggers().add(tagger);
        given(courseService.addTaggerToCourse(Mockito.anyLong(), Mockito.anyLong())).willReturn(course);

        String json = mapper.writeValueAsString(course);

        mockMvc.perform(put("/course/1/tagger/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("taggers").isArray())
                .andDo(print());
    }

    @Test
    void testAddUnit() throws Exception {
        Unit unit = new Unit();
        unit.setTopic("Welcome");
        unit.setReading("Welcome text ...");

        Course course = new Course();
        course.setId(1L);
        course.setName("English World Level 2 Pupil's Book");
        course.setLevel("Level 3");
        course.setCerf("A2");
        course.setCreator("Macmillan Education");
        course.setVersion(1);
        course.setPublication(LocalDate.of(2016, 5, 4));
        course.setDescription("The Pupil's Book contains twelve units with each unit designed to be taught over a two-week period. The key skills of reading, writing, speaking and listening are consistently covered and are underpinned by the firm foundation of the grammar syllabus.");
        course.getUnits().add(unit);
        given(courseService.addUnit(Mockito.anyLong(), Mockito.any())).willReturn(unit);

        String json = mapper.writeValueAsString(unit);

        mockMvc.perform(post("/course/1/unit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("topic").value("Welcome"))
                .andExpect(jsonPath("reading").value("Welcome text ..."))
                .andDo(print());
    }

    @Test
    void testGetUnits() throws Exception {
        Unit unit = new Unit();
        unit.setTopic("Welcome");
        unit.setReading("Welcome text ...");

        Course course = new Course();
        course.setId(1L);
        course.setName("English World Level 2 Pupil's Book");
        course.setLevel("Level 3");
        course.setCerf("A2");
        course.setCreator("Macmillan Education");
        course.setVersion(1);
        course.setPublication(LocalDate.of(2016, 5, 4));
        course.setDescription("The Pupil's Book contains twelve units with each unit designed to be taught over a two-week period. The key skills of reading, writing, speaking and listening are consistently covered and are underpinned by the firm foundation of the grammar syllabus.");
        course.getUnits().add(unit);
        given(courseService.getUnits(Mockito.anyLong())).willReturn(course.getUnits());

        mockMvc.perform(get("/course/1/unit/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andDo(print());
    }
}