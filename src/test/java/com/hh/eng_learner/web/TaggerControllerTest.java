package com.hh.eng_learner.web;

//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hh.eng_learner.entity.Tagger;
import com.hh.eng_learner.exception.EntityNotFoundException;
import com.hh.eng_learner.service.TaggerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TaggerController.class)
class TaggerControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    TaggerService taggerService;

    @Test
    void testSaveTagger() throws Exception {
        Tagger tagger = new Tagger();
        tagger.setId(1L);
        tagger.setTagger("first");
        tagger.setDescription("the first and its desc");
        given(taggerService.saveTagger(Mockito.any())).willReturn(tagger);

        String json = mapper.writeValueAsString(tagger);

        mockMvc.perform(post("/tagger")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andDo(print());
    }

    @Test
    void testGetTaggers() throws Exception {
        Tagger tagger1 = new Tagger();
        tagger1.setId(1L);
        tagger1.setTagger("first");
        tagger1.setDescription("the first and its desc");
        Tagger tagger2 = new Tagger();
        tagger2.setId(1L);
        tagger2.setTagger("second");
        tagger2.setDescription("the second and its desc");
        List<Tagger> taggers = new ArrayList<>();
        taggers.add(tagger1);
        taggers.add(tagger2);
        given(taggerService.getTaggers()).willReturn(taggers);

        mockMvc.perform(get("/tagger/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andDo(print());

    }

    @Test
    void testGetTaggerById() throws Exception {
        Tagger tagger = new Tagger();
        tagger.setId(1L);
        tagger.setTagger("first");
        tagger.setDescription("the first and its desc");
        given(taggerService.getTaggerById(Mockito.any())).willReturn(tagger);

        mockMvc.perform(get("/tagger/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("tagger").value("first"))
                .andExpect(jsonPath("description").value("the first and its desc"))
                .andDo(print());
    }

    @Test
    public void testTaggerNotFoud() throws Exception {
        given(taggerService.getTaggerById(Mockito.any())).willThrow(new EntityNotFoundException(123L, Tagger.class));

        mockMvc.perform(get("/tagger/123"))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    void testUpdateTagger() throws Exception {
        Tagger tagger = new Tagger();
        tagger.setId(1L);
        tagger.setTagger("first");
        tagger.setDescription("the first and its desc");
        given(taggerService.saveTagger(Mockito.any())).willReturn(tagger);

        String json = mapper.writeValueAsString(tagger);

        mockMvc.perform(put("/tagger/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void testDeleteTagger() throws Exception {
        mockMvc.perform(delete("/tagger/1"))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    void testgetTaggerbyTaggerName() throws Exception {
        Tagger tagger = new Tagger();
        tagger.setId(1L);
        tagger.setTagger("first");
        tagger.setDescription("the first and its desc");
        given(taggerService.findByTagger(Mockito.anyString())).willReturn(tagger);

        mockMvc.perform(get("/tagger/name/first"))
                .andExpect(status().isOk())
                .andDo(print());
    }
}