package com.hh.eng_learner.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hh.eng_learner.entity.Course;
import com.hh.eng_learner.entity.Tagger;
import com.hh.eng_learner.entity.Unit;
import com.hh.eng_learner.entity.Word;
import com.hh.eng_learner.service.UnitService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UnitController.class)
class UnitControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    UnitService unitService;

    @Test
    void saveUnitWords() throws Exception {
        Tagger tagger = new Tagger();
        tagger.setId(1L);
        tagger.setTagger("first");
        tagger.setDescription("the first and its desc");

        Unit unit = new Unit();
        unit.setTopic("Welcome");
        unit.setReading("Welcome text ...");

        Course course = new Course();
        course.setId(1L);
        course.setName("English World Level 2 Pupil's Book");
        course.setLevel("Level 3");
        course.setCerf("A2");
        course.setCreator("Macmillan Education");
        course.setVersion(1);
        course.setPublication(LocalDate.of(2016, 5, 4));
        course.setDescription("The Pupil's Book contains twelve units with each unit designed to be taught over a two-week period. The key skills of reading, writing, speaking and listening are consistently covered and are underpinned by the firm foundation of the grammar syllabus.");

        course.getTaggers().add(tagger);
        course.getUnits().add(unit);

        Word word1 = new Word();
        word1.setTagger("first");
        word1.setInEstonian("auto");
        word1.setInEnglish("car");

        Word word2 = new Word();
        word2.setTagger("first");
        word2.setInEstonian("vikerkaar");
        word2.setInEnglish("rainbow");

        unit.getWordSet().add(word1);
        unit.getWordSet().add(word2);

        given(unitService.saveUnitWords(Mockito.anyLong(), Mockito.any())).willReturn(unit);
        String json = mapper.writeValueAsString(unit.getWordSet());

        mockMvc.perform(post("/unit/1/words")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("wordSet").isArray())
                .andDo(print());
    }
}